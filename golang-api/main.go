package main

import (
	"golang-api/book"
	"golang-api/handler"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/golang-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("DB connection error")
	}

	db.AutoMigrate((&book.Book{}))

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)
	/*
		//to create manual (again)
		bookInput := book.BookInput{
			Title: "Genshin Impact Gnostic Hymn",
			Price: "235000",
		}

		bookService.Create(bookInput)
	*/

	/*
			//search all
			books, err := bookRepository.FindAll()
			for _, book := range books {
				fmt.Println("Title :", book.Title)
			}

			//search 1 ID
			book, err := bookRepository.FindByID(2)
			fmt.Println("Title :", book.Title)

		//create data
		book := book.Book{
			Title:       "Everything is Sucks",
			Description: "Stories about life",
			Price:       150000,
			Discount:    15,
			Rating:      4,
		}

		bookRepository.Create(book)
	*/

	//fmt.Println("Database connection success")

	//CRUD = create - read - update - delete
	/*
		//CREATE DATA MANUAL
		//if there're any change here, db will automatically input new data instead of replacing
		book := book.Book{}
		book.Title = "Outliers"
		book.Price = 150000
		book.Discount = 15
		book.Rating = 4
		book.Description = "Stories about very successful people"

		err = db.Create(&book).Error
		if err != nil {
			fmt.Println("Error creating book record")
		}
	*/

	//err = db.Debug().First(&book).Error  >> to get first book
	//err = db.Debug().Last(&book).Error  >> to get last book
	//err = db.Debug().First(&book, 10).Error  >> to get primary key. 10 = PK
	// take = get 1 random, find= no limit order,
	//where("title = ?", "Outlier") = search the title; where("title = ?", 5) = search the title of 5data

	/*
		//READ DATA MANUAL

		//to get all the data
		var books []book.Book

		err = db.Debug().Find(&books).Error
		if err != nil {
			fmt.Println("Error finding book record")
		}

		for _, b := range books {
			fmt.Println("Title :", b.Title)
			fmt.Println("book object %v", b)
		}
	*/

	/*
		//UPDATE DATA MANUAL
		var book book.Book

		err = db.Debug().Where("id = ?", 1).First(&book).Error
		if err != nil {
			fmt.Println("Error finding book record")
		}

		book.Title = "Chacha"
		err = db.Save(&book).Error
		if err != nil {
			fmt.Println("Error updating book record")
		}
	*/

	/*
		//DELETE DATA MANUAL
		var book book.Book

		err = db.Debug().Where("id = ?", 1).First(&book).Error
		if err != nil {
			fmt.Println("Error finding book record")
		}

		err = db.Delete(&book).Error
		if err != nil {
			fmt.Println("Error deleting book record")
		}
	*/

	router := gin.Default()

	//versioning : supaya versi lama tetap ada, sehingga device lama yg masih pakai versi lama tidak error
	v1 := router.Group("/v1")

	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/hi", bookHandler.HiHandler)
	// v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	// v1.GET("/query", bookHandler.QueryHandler)
	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	//new ver use new var v2 and so on

	// listen and serve on localhost:8080
	router.Run(":8888") //change the port use string ":port" ex.:":8888"
}

/*
	Layer Repository:
	1. main
	2. Handler
	3. service
	4. repository
	5. database
	6. mysql
*/
