package book

// struct for get book
type BookResponse struct {
	ID          int
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"` //to use sub_title instead of subtitl
	Price       int    `json:"price" binding:"required, number"`
	Rating      int    `json:"rating" binding:"required, number"`
	Discount    int
}
