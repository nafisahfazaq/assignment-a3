package book

import (
	"encoding/json"
)

// struct for post book
type BookInput struct {
	ID          int
	Title       string      `json:"title" binding:"required"`
	Description string      `json:"description" binding:"required"` //to use sub_title instead of subtitl
	Price       json.Number `json:"price" binding:"required, number"`
	Rating      json.Number `json:"rating" binding:"required, number"`
	Discount    json.Number
}
